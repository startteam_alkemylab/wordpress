# wordpress
docker service: (docker ps)
  dbwordpress
  wordpress

Database:
MySQL 5.7 
accessi:
 user: 	   root
 password: user00root
 user: 	   user
 password: user00
nome db:   db_wp
Porta:	   3306

Applicazione:
Wordpress 5.2.1
Cartella codice: ./code
xdebug.idekey = netbeans-xdebug

Server:
Ubuntu

## WEB APPLICATION
## wordpress:        http://127.0.0.1
## database port:    3306
## database service: dbwordpress

## START
docker-compose up -d

## STOP
docker-compose down

## ENTRARE NEL CONTAINER
docker exec -it wordpress_wordpress_1 bash

## RIAVVIARE APACHE (web server)
#nel container
service apache2 restart

## SETTARE PERMESSI E PROPRIETARIO APACHE (per ambienti remoti)
find . -type f -exec chmod 644 {} \;
find . -type d -exec chmod 755 {} \;
chown -R www-data:www-data /var/www/html

## SETTARE PERMESSI E PROPRIETARIO APACHE (per ambienti locali di sviluppo)
chmod 777 -R /var/www/html
chown -R www-data:www-data /var/www/html

